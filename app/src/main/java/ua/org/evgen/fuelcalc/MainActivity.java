package ua.org.evgen.fuelcalc;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {

    public EditText expenceEditText, priceEditText, distanceEditText;
    public TextView resultTextView;
    public ImageButton shareImageButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        expenceEditText = (EditText)findViewById(R.id.expenceEditText);
        priceEditText = (EditText)findViewById(R.id.priceEditText);
        distanceEditText = (EditText)findViewById(R.id.distanceEditText);
        resultTextView = (TextView)findViewById(R.id.resultTextView);
        shareImageButton = (ImageButton)findViewById(R.id.shareImageButton);

        shareImageButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void clearButtonClick (View view) {
        expenceEditText.setText(getString(R.string.edit_expence_default));
        priceEditText.setText(getString(R.string.edit_price_default));
        distanceEditText.setText(getString(R.string.edit_distance_default));
        resultTextView.setText("");
        shareImageButton.setVisibility(View.INVISIBLE);
    }

    public void calcButtonClick (View view) {
        Float expence = Float.valueOf(expenceEditText.getText().toString());
        Float price = Float.valueOf(priceEditText.getText().toString());
        Float distance = Float.valueOf(distanceEditText.getText().toString());

        Float fuel = (expence / 100) * distance;
        Float cost = fuel * price;

        String resultStr = getString(R.string.need_fuel) + " " + fuel.toString() + " " + getString(R.string.litres_label) + "\n" + getString(R.string.cost_label) + cost.toString() + " " + getString(R.string.currency_name);
        resultTextView.setText(resultStr);
        shareImageButton.setVisibility(View.VISIBLE);
    }

    public void shareImageButtonClick (View view) {
        String shareContent = "";
        shareContent += getString(R.string.label_expence) + " " + expenceEditText.getText().toString() + " " + getString(R.string.label_l100) + "\n";
        shareContent += getString(R.string.label_price) + " " + priceEditText.getText().toString() + " " + getString(R.string.label_grn) + "\n";
        shareContent += getString(R.string.label_distance) + " " + distanceEditText.getText().toString() + " " + getString(R.string.label_km) + "\n";
        shareContent += resultTextView.getText().toString();

        Intent share = new Intent(Intent.ACTION_SEND);
        share.setType("text/plain");
        share.putExtra(Intent.EXTRA_TEXT, shareContent);
        startActivity(Intent.createChooser(share, getString(R.string.share_label)));
    }
}
